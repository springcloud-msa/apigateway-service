package com.example.apigateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class LoggingFilter extends AbstractGatewayFilterFactory<LoggingFilter.Config> {

	public LoggingFilter() {
		super(Config.class);
	}
	@Override
	public GatewayFilter apply(Config config) {
		GatewayFilter filter = new OrderedGatewayFilter(
		 (exchange, chain) -> {
			//request filter
			ServerHttpRequest request = exchange.getRequest();
			log.info("Logging Message : "+ config.getBaseMessage());
			if(config.isPreLogger()) {
//				System.out.println("Global PRE Filter : request id -> "+ request.getId());
				log.info("Logging PRE Filter : request id ->{}", request.getId());
			}
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
			//response filter	
				ServerHttpResponse response = exchange.getResponse();
//				System.out.println("Global POST Filter : response code ->"+response.getStatusCode());
			 if(config.isPostLotgger())	
				log.info("Logging POST Filter : response code ->{}",response.getStatusCode());
			}));
		},Ordered.HIGHEST_PRECEDENCE);
		
		return filter;
	}
	
	@Data
    public static class Config {
        //Put the configuration properties for your filter here
    	private String baseMessage;
    	private boolean preLogger;
    	private boolean postLotgger;
    }
}
