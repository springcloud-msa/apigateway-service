package com.example.apigateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class GlobalFilter extends AbstractGatewayFilterFactory<GlobalFilter.Config> {

	public GlobalFilter() {
		super(Config.class);
	}
	@Override
	public GatewayFilter apply(Config config) {
		
		return (exchange, chain) -> {
			//request filter
			ServerHttpRequest request = exchange.getRequest();
			log.info("GlobalFilter Message : "+ config.getBaseMessage());
			if(config.isPreLogger()) {
//				System.out.println("Global PRE Filter : request id -> "+ request.getId());
				log.info("Global PRE Filter : request id ->{}", request.getId());
			}
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
			//response filter	
				ServerHttpResponse response = exchange.getResponse();
//				System.out.println("Global POST Filter : response code ->"+response.getStatusCode());
			 if(config.isPostLotgger())	
				log.info("Global POST Filter : response code ->{}",response.getStatusCode());
			}));
		};
	}
	
	@Data
    public static class Config {
        //Put the configuration properties for your filter here
    	private String baseMessage;
    	private boolean preLogger;
    	private boolean postLotgger;
    }
}
