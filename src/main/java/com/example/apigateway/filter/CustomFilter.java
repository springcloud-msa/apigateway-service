package com.example.apigateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
//@Slf4j
public class CustomFilter extends AbstractGatewayFilterFactory<CustomFilter.Config> {

	public CustomFilter() {
		super(Config.class);
	}
	@Override
	public GatewayFilter apply(Config config) {
		
		return (exchange, chain) -> {
			//request filter
			ServerHttpRequest request = exchange.getRequest();
			System.out.println("Customer PRE Filter : request id -> "+ request.getId());
//			log.info("Customer PRE Filter : request id ->{}", request.getId());
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
			//response filter	
				ServerHttpResponse response = exchange.getResponse();
				System.out.println("Customer POST Filter : response code ->"+response.getStatusCode());
//				log.info("Customer POST Filter : response code ->{}",response.getStatusCode());
			}));
		};
	}
    public static class Config {
        //Put the configuration properties for your filter here
    }
}
